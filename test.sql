-- Create tables representing models
CREATE TABLE IF NOT EXISTS TestModel (
	ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	Name TEXT NOT NULL,
	Stars INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS TestModelSubData (
	TestModelID INTEGER,
	HeightInches INTEGER NOT NULL,
	WeightLbs INTEGER NOT NULL,
	Alias TEXT NOT NULL
);

-- Delete old data
DELETE FROM TestModel;
DELETE FROM TestModelSubData;

-- Insert some bogus data
INSERT INTO TestModel (ID, Name, Stars) VALUES (1, 'Chris Eaton', 3);
INSERT INTO TestModel (ID, Name, Stars) VALUES (2, 'Floki', 8);
INSERT INTO TestModel (ID, Name, Stars) VALUES (3, 'Jarl Borg', 10);
INSERT INTO TestModel (ID, Name, Stars) VALUES (4, 'Siggy', 10);
INSERT INTO TestModel (ID, Name, Stars) VALUES (5, 'Gunther', 1);
INSERT INTO TestModel (ID, Name, Stars) VALUES (6, 'Finn Human', 5);
INSERT INTO TestModel (ID, Name, Stars) VALUES (7, 'Jake Dog', 7);
INSERT INTO TestModel (ID, Name, Stars) VALUES (8, 'P. Bubs', 8);
INSERT INTO TestModel (ID, Name, Stars) VALUES (9, 'Athelstan', 6);
INSERT INTO TestModel (ID, Name, Stars) VALUES (10, 'Rollo', 9);
INSERT INTO TestModel (ID, Name, Stars) VALUES (11, 'Ragnar', 10);
INSERT INTO TestModel (ID, Name, Stars) VALUES (12, 'Lagertha', 2);
INSERT INTO TestModel (ID, Name, Stars) VALUES (13, 'Bjorn', 3);
INSERT INTO TestModel (ID, Name, Stars) VALUES (14, 'Starchy', 4);

INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (1, 71, 230, "the Dev Expert");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (2, 74, 220, "Loki");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (3, 72, 220, "Jarl");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (4, 62, 220, "");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (5, 30, 220, "Gunter");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (6, 62, 220, "Finn");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (7, 52, 220, "Bro");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (8, 69, 220, "Bubblegum");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (9, 67, 220, "Christian");
INSERT INTO TestModelSubData (TestModelID, HeightInches, WeightLbs, Alias) VALUES (10, 71, 220, "Traitor");