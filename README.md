![malk sqlite storage](https://gitlab.com/uploads/project/avatar/1038919/malk-storage-sqlite.png "malk sqlite storage")
# [malk-storage-sqlite](https://gitlab.com/chriseaton/malk-storage-sqlite)

This is a storage driver for [Malk](https://gitlab.com/chriseaton/malk) (content management system). Using this driver
provides Malk with the ability to save all data in a SQLite database.

# Usage
TODO