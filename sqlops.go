/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage-sqlite
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package sqlitestorage

import (
	"database/sql"
	"fmt"
	"gitlab.com/chriseaton/malk-storage"
	"reflect"
	"strings"
	"unicode"
)

const (
	sqlEntityTable       = "CREATE TABLE IF NOT EXISTS %s (%s\n);\n\n"
	sqlEntityTableColumn = "\n\t%s %s NOT NULL," //yep all columns are not null to facilitate the use of concrete querying
)

type sqlOps struct{}

// Opens a connection to the sqlite database file with the specified mode.
func (ops *sqlOps) Connect(fp string, mode string) (*sql.DB, error) {
	if fp == "" {
		return nil, fmt.Errorf("File path cannot be blank")
	}
	return sql.Open("sqlite3", fmt.Sprintf("file:%s?cache=shared&mode=%s", fp, mode))
}

// Creates a table for the specified entity if it doesn't already exist using the model structure. Sub-struct values
// will be created as seperate tables with a reference column (ID) to the parent entity.
func (ops *sqlOps) ScriptCreateTable(tableName string, parentTableName string, m interface{}) (string, error) {
	if m == nil {
		return "", fmt.Errorf("Input structure \"m\" cannot be nil.")
	}
	// create table script based on model
	var script string
	var cols string
	rv := reflect.Indirect(reflect.ValueOf(m))
	if storage.IsValidModelKind(rv.Kind()) {
		if parentTableName != "" {
			//Include a parent table reference column
			cols += fmt.Sprintf(sqlEntityTableColumn, parentTableName+"ID", "UNSIGNED BIG INT")
		} else {
			cols += fmt.Sprintf(sqlEntityTableColumn, "ID", "INTEGER PRIMARY KEY AUTOINCREMENT")
		}
		//loop through fields to create script
		for i := 0; i < rv.NumField(); i++ {
			fv := rv.Field(i)
			sf := rv.Type().Field(i)
			exported := ([]rune(sf.Name)[0] == unicode.ToUpper([]rune(sf.Name)[0]))
			if fv.IsValid() && sf.Anonymous == false && exported {
				fk := fv.Kind()
				if storage.IsValidModelKind(fk) {
					// handle model sub-structure
					ts, err := ops.ScriptCreateTable(tableName+sf.Name, tableName, fv.Interface())
					if err != nil {
						return "", err
					}
					script += ts
				} else if storage.IsValidModelFieldKind(fk) {
					// script out the column
					var colType string
					switch fk {
					case reflect.Bool:
						colType = "BOOLEAN"
					case reflect.String:
						colType = "TEXT"
					case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32,
						reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32:
						colType = "INTEGER"
					case reflect.Int64:
						colType = "BIGINT"
					case reflect.Uint64:
						colType = "UNSIGNED BIG INT"
					case reflect.Float32, reflect.Float64:
						colType = "REAL"
					default:
						return "", fmt.Errorf(storage.ErrorModelFieldKindNotSupported, fk)
					}
					cols += fmt.Sprintf(sqlEntityTableColumn, sf.Name, colType)
				} else {
					return "", fmt.Errorf("Input model %q field %q is not defined with a type supported by Malk (%q).", rv.Type().Name(), sf.Name, fk)
				}
			}
		}
	} else {
		return "", fmt.Errorf("Input model structure %q is not supported by Malk.", rv.Kind())
	}
	script += fmt.Sprintf(sqlEntityTable, tableName, strings.TrimRight(cols, ",\n"))
	return script, nil
}

func (ops *sqlOps) ScriptSave(tableName string, parentTableName string, m interface{}) (string, error) {
	return "", nil
}

// Generates a sql where "IN" clause. Returns the string clause.
// For example SqlWhereIn("Stars", []uint64{1,2,3}) will return "Stars IN (1,2,3)".
func (ops *sqlOps) SqlWhereIn(col string, numbers []uint64) string {
	return fmt.Sprintf("%s IN (%s)", col, strings.Trim(strings.Replace(fmt.Sprintf("%v", numbers), " ", ",", -1), "[]"))
}
