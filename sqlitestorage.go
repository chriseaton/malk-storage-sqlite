/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage-sqlite
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package sqlitestorage

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/chriseaton/malk-storage"
	"path/filepath"
	"regexp"
	"strings"
)

var invalidTableNameRunes = regexp.MustCompile("[^A-z0-9-_]")

// A standard error that also includes an index value for work with a slice.
type SliceError struct {
	Index   int
	Message string
}

// Returns the error message
func (se *SliceError) Error() string {
	return se.Message
}

// Returns the table name for a given entity.
func TableName(c Configuration, entity string) string {
	//strip bad characters
	entity = invalidTableNameRunes.ReplaceAllString(strings.Title(entity), "")
	prefix := invalidTableNameRunes.ReplaceAllString(c.TablePrefix, "")
	postfix := invalidTableNameRunes.ReplaceAllString(c.TablePostfix, "")
	//combine with pre/post-fixes
	return fmt.Sprintf("%s%s%s", prefix, entity, postfix)
}

//Creates a new sqlite storage driver for Malk with the specified configuration.
func Create(dbFilePath string) (*Driver, error) {
	if dbFilePath == "" {
		dbFilePath = "./data/db.sqlite"
	}
	d := &Driver{
		Configuration: Configuration{
			DatabaseFilePath: dbFilePath,
		},
	}
	return d, nil
}

// Storage driver for Malk (CMS). Save models to a SQLite3 database.
type Driver struct {

	//Driver configuration
	Configuration Configuration

	// The model registry
	registry *storage.EntityModelRegistry
}

type Configuration struct {

	// The file path for the Sqlite database file (needs read/write access).
	// If this field is left blank "./data/db.sqlite" will be used.
	DatabaseFilePath string

	// Boolean indicating whether to use soft-deletes.
	//
	// If True, the storage driver will use a flag to indicate a row (representing a model) is deleted, rather than
	// actually deleting the row permanently. This allows for data retention of "deleted" data and may be useful for
	// data recovery, but will increase storage space usage.
	//
	// If False (default), the storage driver will delete the row from the table permanently. This makes deleted data
	// recovery more difficult (if not impossible), but helps keep data storage space in check.
	SoftDelete bool

	// A custom prefix applied to all entity table names.
	TablePrefix string

	// A custom postfix applied to all entity table names.
	TablePostfix string
}

// Sets up the storage driver when the server is configured.
func (d *Driver) Configure(c storage.DriverConfiguration, r *storage.EntityModelRegistry) error {
	dc, ok := c.(Configuration)
	if ok {
		d.Configuration = dc
	}
	//ensure an absolute file path
	if d.Configuration.DatabaseFilePath == "" {
		d.Configuration.DatabaseFilePath = "./data/db.sqlite"
	}
	p, err := filepath.Abs(d.Configuration.DatabaseFilePath)
	if err != nil {
		return fmt.Errorf("Unable to create absolute path from path %q.", d.Configuration.DatabaseFilePath)
	}
	d.Configuration.DatabaseFilePath = p
	//make sure prefix and postfix only contain valid runes
	if d.Configuration.TablePrefix != invalidTableNameRunes.ReplaceAllString(d.Configuration.TablePrefix, "") {
		return fmt.Errorf("TablePrefix contains invalid characters. Only letters, numbers, dashes, and underscores are allowed.")
	} else if d.Configuration.TablePostfix != invalidTableNameRunes.ReplaceAllString(d.Configuration.TablePostfix, "") {
		return fmt.Errorf("TablePostfix contains invalid characters. Only letters, numbers, dashes, and underscores are allowed.")
	}
	if r == nil {
		return fmt.Errorf("The registry argument must be specified.")
	}
	d.registry = r
	//create the database.
	ops := &sqlOps{}
	db, err := ops.Connect(d.Configuration.DatabaseFilePath, "rwc")
	if err != nil {
		return err
	}
	defer db.Close() //close up connection
	//ensure tables for all registered data models.
	var script string
	for _, e := range d.registry.Entities() {
		ts, err := ops.ScriptCreateTable(TableName(d.Configuration, e), "", d.registry.New(e))
		if err != nil {
			return err
		}
		script += ts
	}
	fmt.Println(script)
	_, err = db.Exec(script)
	return err
}

// Saves the model to the database, assigning the model a new id if necessary.
func (d *Driver) Save(entity string, m storage.Model) (uint64, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return 0, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//save
	return d.saveToDatabase(nil, TableName(d.Configuration, entity), m)
}

// Saves the slice of models to the database, assigning new id's as necessary.
func (d *Driver) SaveAll(entity string, models ...storage.Model) error {
	//open db connection
	ops := &sqlOps{}
	db, err := ops.Connect(d.Configuration.DatabaseFilePath, "rw")
	if err != nil {
		return err
	}
	defer db.Close()
	//begin saving each model
	tn := TableName(d.Configuration, entity)
	for i, m := range models {
		_, err := d.saveToDatabase(db, tn, m)
		if err != nil {
			return &SliceError{
				Index:   i,
				Message: fmt.Sprintf("Error saving model at index: %v.\nError: %v", i, err),
			}
		}
	}
	return nil
}

// Underlying sql operation to save, accepts an already open database connection.
func (d *Driver) saveToDatabase(db *sql.DB, tableName string, m storage.Model) (uint64, error) {
	if db == nil {
		//open db connection
		ops := &sqlOps{}
		db, err := ops.Connect(d.Configuration.DatabaseFilePath, "rw")
		if err != nil {
			return 0, err
		}
		defer db.Close()
	}
	//make update call, if 0 rows affected, we need to insert.
	//TODO
	return 0, nil
}

// Retrieves the model, specified by the ID, from the database.
func (d *Driver) Get(entity string, id uint64) (storage.Model, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return nil, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//TODO
	return nil, nil
}

// Retrieves the models as specified by their particular ID from the database.
func (d *Driver) GetAll(entity string, ids ...uint64) (models []storage.Model, err error) {
	//TODO
	return nil, nil
}

// Checks if an entity model exists for each id specified.
// If any id is not found, False is returned. If all ids have a matching model, True is returned.
func (d *Driver) Exists(entity string, ids ...uint64) (bool, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return false, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//open db connection
	ops := &sqlOps{}
	db, err := ops.Connect(d.Configuration.DatabaseFilePath, "rw")
	if err != nil {
		return false, err
	}
	defer db.Close()
	//check for rows
	qry := fmt.Sprintf("SELECT COUNT(ID) FROM %s WHERE %s;", TableName(d.Configuration, entity), ops.SqlWhereIn("ID", ids))
	count := 0
	err = db.QueryRow(qry).Scan(&count)
	if err != nil {
		return false, err
	}
	return (count == len(ids)), nil
}

// Searches the database for entity models that match the given query criteria.
func (d *Driver) Query(qry *storage.Query) ([]storage.Model, error) {
	if qry == nil {
		return nil, fmt.Errorf("The \"qry\" argument must be specified.")
	} else if err := qry.Validate(); err != nil {
		return nil, err
	}
	//TODO
	return nil, nil
}

// Searches the database and returns only the specified field values.
// Returns a QueryFieldResult map (map[uint64]map[string]interface{}) and an error if one occurred.
func (d *Driver) QueryFields(qry *storage.Query, fields ...string) (storage.QueryFieldResultMap, error) {
	if qry == nil {
		return nil, fmt.Errorf("The \"qry\" argument must be specified.")
	} else if err := qry.Validate(); err != nil {
		return nil, err
	} else if fields == nil || len(fields) == 0 {
		return nil, fmt.Errorf("The \"fields\" argument must be specified.")
	}
	//TODO
	return nil, nil
}

// Deletes the model with the specified id from the database.
// Returns True if the file was found and deleted. Returns False if the file couldn't be deleted (not found or error)
func (d *Driver) Delete(entity string, id uint64) (bool, error) {
	//validate entity
	if d.registry.Exists(entity) == false {
		return false, fmt.Errorf(storage.ErrorEntityNotRegistered, entity)
	}
	//TODO
	return false, nil
}

// Permanently deletes all model files with the matching id values. If an error occurs, a SliceError is returned
// deletion of further models is halted.
func (d *Driver) DeleteAll(entity string, ids ...uint64) error {
	//TODO
	return nil
}
