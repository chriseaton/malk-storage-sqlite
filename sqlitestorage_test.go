/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage-sqlite
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package sqlitestorage

import (
	"flag"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/chriseaton/malk-storage"
	"io/ioutil"
	"os"
	"testing"
)

const (
	TestDataDirectory = "./test/"
)

// Model for testing
type TestModel struct {
	uniqueID uint64
	Name     string
	Stars    int
	SubData  struct {
		HeightInches uint8
		WeightLbs    uint8
		Alias        string
	}
}

// function for Model interface
func (m *TestModel) ID() uint64 {
	return m.uniqueID
}

// function for Model interface
func (m *TestModel) SetID(id uint64) {
	m.uniqueID = id
}

func TestMain(m *testing.M) {
	noDelTestDir := flag.Bool("nodel", false, "Don't delete the test data directory when tests complete.")
	flag.Parse()
	var result int
	if err := os.Mkdir(TestDataDirectory, 0774); err != nil && !os.IsExist(err) {
		fmt.Printf("Failed to create test directory. Error: %s", err)
		os.Exit(1)
	} else {
		//get db script
		sb, err := ioutil.ReadFile("./test.sql")
		if err != nil {
			fmt.Printf("Error reading \"test.sql\" file.\nError: %s", err)
			os.Exit(1)
		}
		//create test database
		dbFileName := TestDataDirectory + "testdb.sqlite"
		ops := &sqlOps{}
		db, err := ops.Connect(dbFileName, "rwc")
		defer db.Close()
		if err != nil {
			fmt.Printf("Error opening connection to sql file %q.\nError: %s", dbFileName, err)
			os.Exit(1)
		}
		_, err = db.Exec(string(sb))
		if err != nil {
			fmt.Printf("Error running \"test.sql\" script on database.\nError: %s", err)
			os.Exit(1)
		}
		//run tests
		result = m.Run()
	}
	if *noDelTestDir == false {
		if err := os.RemoveAll(TestDataDirectory); err != nil && !os.IsNotExist(err) {
			fmt.Printf("Failed to delete test directory. Error: %s\n", err)
		}
	}
	os.Exit(result)
}

func TestExists(t *testing.T) {
	d, err := CreateTestDriver()
	if err != nil {
		t.Errorf("%s", err)
	}
	//test exists call for valid ids
	c, err := d.Exists("testmodel", 1, 2, 3)
	assert.NoError(t, err, "Exist should not return an error when arguments are valid.")
	assert.True(t, c, "Exists expected to return true when all IDs are valid.")
	//test exists call for some valid and invalid ids
	c, err = d.Exists("testmodel", 1, 2, 243, 8)
	assert.NoError(t, err, "Exist should not return an error when arguments are valid.")
	assert.False(t, c, "Exists expected to return false when some IDs are missing.")
	//test exists call for invalid ids
	c, err = d.Exists("testmodel", 150, 220, 138)
	assert.NoError(t, err, "Exist should not return an error when arguments are valid.")
	assert.False(t, c, "Exists expected to return false when all IDs are missing.")
}

func CreateTestDriver() (*Driver, error) {
	d, err := Create(TestDataDirectory + "testdb.sqlite")
	if err != nil {
		return nil, fmt.Errorf("Error creating driver. Error: %s", err)
	} else {
		if err = d.Configure(nil, &storage.EntityModelRegistry{}); err != nil {
			return nil, fmt.Errorf("Error configuring driver. Error: %s", err)
		}
		d.registry.Add("testmodel", func() storage.Model { return &TestModel{} })
		return d, nil
	}
}
